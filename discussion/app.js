db.fruits.insertMany([
    {
        "name": "Apple",
        "color": "Red",
        "stock": 20,
        "price": 40,
        "onSale": true,
        "origin": ["Philippines", "US"]
    },
    {
        "name": "Banana",
        "color": "Yellow",
        "stock": 15,
        "price": 20,
        "onSale": true,
        "origin": ["Philippines", "Ecuador"]
    },
    {
        "name": "Kiwi",
        "color": "Green",
        "stock": 25,
        "price": 50,
        "onSale": true,
        "origin": ["US", "China"]
    }, 
    {
        "name": "Mango",
        "color": "Yellow",
        "stock": 10,
        "price": 120,
        "onSale": false,
        "origin": ["Philippines", "India"]
    }
]);

// Aggregation Syntax
db.collections.aggregate(
    [
        {stage1},
        {stage2},
        {stage3}
    ]
    )
    
    // Stages
    // $match
    // Filters the documents to pass only the documents that match the specified condition(s) to the next pipeline stage.
    // Syntax:
    {$match: {query}}
    
    // mini activity
    // group together documents that fruits are currently on sale.
    db.fruits.aggregate(
        [
            {$match: {"onSale": true}},
            {$count: "fruitCount"}
        ]
        );
        // $count
        // Returns a count of the number of document at this stage of the aggregation pipeline.
        // Syntax: 
        {$count: string}
        
        // mini activity
        db.fruits.aggregate(
        [
            { $match: { onSale: true } },
            { $count: "fruitCount" }
        ]
        );
        
        // $group
        // Groups input documents by a specified identifier expression and applies the accumulator expression(s), if specified, to each group. Consumes all input documents and outputs one document per each distinct group. The output documents only contain the identifier field and, if specified, accumulated fields.
        // Syntax:
        {
            $group:
            {
                _id: expression, // Group By Expression
                field1: { accumulator1 : expression1 },
                ...
            }
        }
        // mini activity
        db.fruits.aggregate(
        [
            { $match: { onSale: true } },
            {
             $group: 
               { 
                _id: null,
                "total": {$sum: "$stock"}
               }
            }
        ]
        );

    // $project
        // Reshapes each document in the stream, such as by adding new fields or removing existing fields. For each input document, outputs one document.
        // Syntax:
            { $project: { <specification(s)> } }

            // mini activity
                // group together
              db.fruits.aggregate(
                  [
                    {
                        $match: {"stock": {$gt: 10}}
                    },
                    {
                        $project: {"_id": 0}
                    },
                    {
                        $sort: {"price": 1}
                    }
                  ]
                );

    //$unwind
		// syntax:
			{
			$unwind:
				{
				path: field path,
				includeArrayIndex: string,
				preserveNullAndEmptyArrays: boolean
				}
			}

			db.fruits.aggregate(
				[
					{$unwind: "$origin"}, //deconstruct the array
					{$group: 
						{_id: "$origin", //group together same origin
						 kinds: {$sum: 1 } //adds
						}
					}	
				]
			)

			
        