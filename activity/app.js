// Solution #2
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnSale" }]);

// Solution #3
db.fruits.aggregate([
	{ $match: { stock: { $gt: 20 } } },
	{ $count: "enoughStock" },
]);

// Solution #4
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: null, avg_price: { $avg: "$price" } } },
]);

// Solution #5
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{
		$group: {
			_id: null,
			max_price: { $max: "$price" },
		},
	},
]);

// Solution #6
db.fruits.aggregate([
	{ $match: { onSale: true } },
	{
		$group: {
			_id: null,
			min_price: { $min: "$price" },
		},
	},
]);
